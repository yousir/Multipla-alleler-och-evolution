#!/usr/bin/env python3
import matplotlib.pyplot as plt
from openpyxl import Workbook

class Graf:
    def __init__(self, stream, spara=True, fn=None):
        x,y = ([],[])
        for data in stream:
            for k,v in data.items():
                x.append(k)
                y.append(v['LETALA'])

        plt.plot(x, y)
        plt.xlim([-1,max(x)+1])
        plt.ylim([-1,max(y)+1])
        plt.xlabel('Generationer')
        plt.ylabel('Letala kombinationer')
        plt.title('Evolution')
        plt.grid(True)
        if spara:
            if not fn:
                fn = 'diagram.png'
            plt.savefig(fn)
        plt.show()

class Xls:
    def __init__(self, stream, fn=None):
        wb = Workbook()
        ws = wb.active

        ws['A1'] = 'Generation'
        ws['A2'] = 'Antal (S) och (l) alleler i varje generation'
        ws['A3'] = 'Antal letala kombinationer (individer)'

        for data in stream:
            for k,v in data.items():
                ws['{}1'.format(chr(ord('a') + k+1).upper())] = k
                cell = ws['{}1'.format(chr(ord('a') + k).upper())]
                cell.font = cell.font.copy(bold=True)
                ws['{}2'.format(chr(ord('a') + k+1).upper())] = v['Sl']
                ws['{}3'.format(chr(ord('a') + k+1).upper())] = v['LETALA']


        if not fn:
            fn = 'resultattabell.xlsx'
        elif '.xlsx' not in fn.rsplit('.',1)[-1]:
            fn = fn + '.xlsx'
        wb.save(fn)
