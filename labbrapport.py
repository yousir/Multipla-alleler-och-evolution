#!/usr/bin/env python3
from __init__ import Generation
from util import Graf, Xls
import argparse, os

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Labboration: Multipla alleler och evolution')
    parser.add_argument('-f', '--filnamn', action='store', dest='fn', default=False, type=str,
                        help='Spara till [FN] i mappen \'./export\'.'
                        ' --spara anropas automatiskt.')
    parser.add_argument('-s', '--spara', action='store_true', default=False,
                        help='Spara till fil i mappen \'export\'.')
    parser.add_argument('-ld', '--linjediagram', action='store_true', dest='ld', default=False,
                        help='Rita ett linje-diagram.')
    parser.add_argument('-pd', '--punktdiagram', action='store_true', dest='pd', default=False,
                        help='Rita ett punkt-diagram.')
    parser.add_argument('-x', '--excel', action='store_true', dest='xls', default=False,
                        help='Skapa ett kalkylark i xls-format.'
                        ' --spara anropas automatiskt.')

    args = parser.parse_args()

    if args.fn:
        args.spara = True
    if args.spara or args.xls:
        os.makedirs('./export', exist_ok=True)
        os.chdir('./export') 

    run = Generation()
    run.selektion(args.spara, args.fn)
    
    if args.ld or args.pd:
        Graf(run.data(), args.spara, args.fn, args.ld, args.pd)
    if args.xls:
        Xls(run.data(), args.fn)
