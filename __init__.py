#!/usr/bin/env python3
import numpy.random as np
import os

class Generation:
    def __init__(self):
        np.seed(19680801)
        self.kulor, self.urval = ['S']*130 + ['l']*130 + ['r']*130, []
        self.generationer = {}
        self.gen = -1

    def genpool(self):
        np.shuffle(self.kulor) # blanda om vid varje påfyllning?

        try:
            self.urval.extend([self.kulor.pop(np.randint(len(self.kulor))) for _ in range(50-len(self.urval))])
        except ValueError:
            pass

        i = iter(self.urval)
        a = next(i, None)
        for n,b in enumerate(i):
            yield (a, b)
            a = b
            self.urval.pop(n)
            
    def selektion(self, spara=False, fn=None):
        if spara:
            if not fn:
                fn = 'data.txt'
            if os.path.isfile(fn):
                os.remove(fn)
        while True:
            self.gen += 1
            if self.gen == 14:
                break            
            self.generationer[self.gen] = {'Sl': 0, 'r': 0, 'EJ LETALA': 0, 'LETALA': 0, 'PAR': 0, 'TOTALT': 0}
            kombo = [x for x in self.genpool()]
            self.generationer[self.gen]['PAR'] = len(kombo)
            for par in kombo:
                if par[0] == par[1] and 'r' not in par:
                    self.generationer[self.gen]['LETALA'] += 1
                else:
                    self.generationer[self.gen]['EJ LETALA'] += 1

                for x in par:
                    self.generationer[self.gen]['TOTALT'] += 1
                    if x == 'r':
                        self.generationer[self.gen]['r'] += 1
                    if x in ['S', 'l']:
                        self.generationer[self.gen]['Sl'] += 1
            msg = 'Gen: {0} \nLETALA: {1} \nEJ LETALA: {2} \nPAR: {3} \nS och l: {4} \nr: {5} \nTOTALT: {6}\n'.format(self.gen,
                                                                                                                        self.generationer[self.gen]['LETALA'],
                                                                                                                        self.generationer[self.gen]['EJ LETALA'],
                                                                                                                        self.generationer[self.gen]['PAR'],
                                                                                                                        self.generationer[self.gen]['Sl'],
                                                                                                                        self.generationer[self.gen]['r'],
                                                                                                                        self.generationer[self.gen]['TOTALT'])
            print(msg)
            if spara:
                with open(fn, 'a') as f:
                    f.write(msg)

    def data(self):
        yield self.generationer
